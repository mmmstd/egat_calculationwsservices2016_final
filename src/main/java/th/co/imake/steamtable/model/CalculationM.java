package th.co.imake.steamtable.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by imake on 03/02/2016.
 */
public class CalculationM extends FormatM implements Serializable {
    private List<FormulaM> formula;

    public List<FormulaM> getFormula() {
        return formula;
    }

    public void setFormula(List<FormulaM> formula) {
        this.formula = formula;
    }
}
