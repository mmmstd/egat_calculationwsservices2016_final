package th.co.imake.steamtable.model;

import java.io.Serializable;

/**
 * Created by imake on 03/02/2016.
 */
public class FormulaM implements Serializable {
    private String key;
    private String value;
    private String result;
    private String status;
    private String time;
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
