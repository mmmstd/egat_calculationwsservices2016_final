package th.co.imake.steamtable.rest.resource;

import org.apache.commons.io.IOUtils;
import org.restlet.data.CharacterSet;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.representation.ByteArrayRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

/**
 * Created by imake on 30/06/2016.
 */
public class DownloadFileResource extends BaseResource {
   // private static String  ROOT_PATH="/Users/imake/PhpstormProjects/git_project/ais_v3/public/exportData/";
   private static String  ROOT_PATH="/var/www/ais_v3/public/exportData/";
    @Autowired
    private com.thoughtworks.xstream.XStream jsonXstream;

    public DownloadFileResource() {
        super();
        logger.debug("into DownloadFileResource");
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doInit() throws ResourceException {
        // TODO Auto-generated method stub
        super.doInit();
        logger.debug("into doInit");
    }

    @Override
    protected Representation get(Variant variant) throws ResourceException {
        Representation rep=null;
        String filename= getQuery().getValues("filename");
        String filetype= getQuery().getValues("filetype");
        org.joda.time.DateTime dt1 = new org.joda.time.DateTime(new Date().getTime());

        String monthStr = dt1.getMonthOfYear() + "";
        String yearStr = dt1.getYear() + "";
        int day=dt1.getDayOfMonth();
        String dayStr=day>9?(day+""):("0"+day);
        monthStr = monthStr.length() > 1 ? monthStr : "0" + monthStr;
        File someFile = new File(ROOT_PATH+"/"+yearStr + "_" + monthStr + "_"+dayStr+"/"+filename+"."+filetype);
        FileInputStream fin=null;
        try {
             fin= new FileInputStream(someFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        byte[] fileContent=null;
        try {
            fileContent=IOUtils.toByteArray(fin);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            // rep=new FileRepresentation(new File(wb.getBytes()), MediaType.APPLICATION_OCTET_STREAM);
            rep=new ByteArrayRepresentation(fileContent, MediaType.APPLICATION_OCTET_STREAM);
            rep.setCharacterSet(CharacterSet.UTF_8);
            Disposition disposition=new Disposition();

            filename="ExportData";
            disposition.setType(Disposition.TYPE_ATTACHMENT);
            disposition.setFilename(filename+"."+filetype);
            rep.setDisposition(disposition);

        } finally {

            try {
                fin.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // if(someFile.exists())
            //    someFile.delete();
        }

       /*
        ServletOutputStream servletOutputStream = null;
        try
        {
            servletOutputStream = response.getOutputStream();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            wb.write(servletOutputStream);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            servletOutputStream.flush();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            servletOutputStream.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        */
        return rep;
        //return null;
    }

}
